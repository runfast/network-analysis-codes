import numpy as np
import networkx as nx
import itertools as it
import operator
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg.matfuncs import fractional_matrix_power
from multiprocessing import Pool

# In this function we define the network of size n with num_1 as the number of nodes that have only 1 edge...
# The remainder will have 3 edges according to the problem.  The edges are then randomly permuted and an edge...
# list is built going through several conditional statements to ensure no multi-edges or self loops.  This was...
# originally done with a nxn search which proved 72x slower as it had a O(n^2) vs O(n) with the for loops
def gen_re(p_1):
   n = 10000
   # get number vertices degree 1 and number vertices degree 3
   num_1 = int(n * p_1 / 100)
   graph_array = []
   for i in range(0, num_1):
      graph_array.append(i)
   for i in range(num_1, n):
      graph_array.extend((i,i,i))
   random_perm = list(np.random.permutation(graph_array))
   random_edges = []
   while len(random_perm)>1:
      i_1 = random_perm.pop(0)
      i_2 = random_perm.pop(0)
      # if i_1 != i_2 and (i_1, i_2) not in random_edges and (i_2, i_1) not in random_edges:
      #    random_edges.append((i_1,i_2))
      if i_1 != i_2:
         random_edges.append((i_1, i_2))
   random_edges = sorted(random_edges)
   x = 0
   rev = []
   edges = []
   while random_edges:
      i = random_edges.pop(0)
      if random_edges:
         j = random_edges.pop(0)
      else:
         j = i
      if i == x and i == j:
         continue
      elif i == j:
         edges.append(i)
      elif i == x:
         edges.append(j)
      else:
         edges.append(i)
         edges.append(j)
      x = j
   for i in edges:
      rev.append((i[1], i[0]))
   rev = sorted(rev)
   full = [[i] for i in range(10000)]
   for i in rev:
      full[i[0]].extend([i[1]])
   for i in edges:
      lenfull = len(full[i[0]])
      if lenfull == 4:
         a = full[i[0]][1]
         b = full[i[0]][2]
         c = full[i[0]][3]
      elif lenfull == 3:
         a = full[i[0]][1]
         b = full[i[0]][2]
         c = 0
      elif lenfull == 2:
         a = full[i[0]][1]
         b = 0
         c = 0
      else:
         a = 0
         b = 0
         c = 0
      if i[1] == a or i[1] == b or i[1] == c:
         edges.pop(edges.index(i))
         #print("REVERSE edge popped", i)
   return edges


# Here we build the graph from the associated node and edge list
def build_config_graph(edges, nodes):
   G_config = nx.Graph()
   # has the same vertices
   for v in nodes:
      G_config.add_node(v)
   for e in edges:
      G_config.add_path(e)
   return(G_config)


# Here we gather the mean size of the largest component of the network with p_1% of the nodes having only 1 edge...
# after running a 100 samples of the graph
def mean_fractional(p_1):
   G = nx.Graph()
   fractional_component_lengths = []
   nodes = range(0, 10000)
   for i in range(20):
      G = build_config_graph(gen_re(p_1), nodes)
      giant = sorted(nx.connected_components(G), key=len, reverse=True)
      fractional_component_lengths.append(len(giant[0]))
      #print("build done")
   return(int(np.mean(fractional_component_lengths)))


# Here is the main component of the program where we use parallelization across the rang of p_1 values
if __name__ == '__main__':
   with Pool(8) as p:
      a = p.map(mean_fractional, range(1,7))
      print(a)