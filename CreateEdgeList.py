# This program creates an edge list for all physically possible networks with number of nodes numnodes...
# A list of networks possible by summing the degrees of nodes and following specific rules is first created...
# This initially rejects a large number of networks that are not physically possible...
# Finally a list of edges is created for each network to specify its physical layout...
# In creating the edge list any further network (as a list of node degrees) not physically possible is rejected

import math
import itertools

# Here we set the number of nodes in the networks we would like to build out
numnodes = 5

# Here we initialize network parameter variables
minedges = int((2 * numnodes - 2) / 2)
maxedges = int((numnodes * numnodes - numnodes) / 2)
numgraphs = math.factorial(numnodes - 1)
maxdegree = numnodes - 1
maxgraphs = (numnodes - 1) ** numnodes
print(minedges, maxedges, numgraphs, maxgraphs)

# Initialize variables and lists for graphs and sums
graphs = [[] for i in range(minedges * 2, maxedges * 2 + 1, 2)]
sums = [i for i in range(minedges * 2, maxedges * 2 + 1, 2)]
#print(sums)

# Here we create all possible networks of size numnodes that follow the rules for degrees adding to sums...
# In short, the smallest network has sum of degrees = 2*numnodes - 2 and largest has numnodes**2 - numnodes...
# Each sum in-between the smallest and largest and that is a multiple of 2 is also a viable sum
verts = [1 for i in range(1, numnodes+1)]
for verts in itertools.combinations_with_replacement(range(1, numnodes), numnodes):
    for i in range(0, len(sums)):
        if sum(verts) == sums[i]:
            graphs[i].append(verts)

# A quick module to determine the number of graphs possible for node size...
# ***This includes some impossible graphs - those aren't removed until edge creation
notgraphs = 0
numgraphs = 0
for i in range(0, len(sums)):
    #print("sum", sums[c])
    #print("Graphs:", graphs[i])
    numgraphs = numgraphs + len(graphs[i])

# Here we create the edge list from the vertex degree list...
# There are several rules that must be followed to ensure a physically possible network graph...
# In general, the lowest degree vertex is joined to the highest degree in the list, assuming there is not yet that edge
for i in range(0, len(sums)):
    for j in range(0, len(graphs[i])):
        nodes = list(graphs[i][j])
        orignodes = [x for x in nodes]
        k = 0
        edges = []
        while sum(nodes) > 0:
            while nodes[k] > 0:
                highs = [n for n , o in enumerate(nodes) if o == max(nodes)]
                right_high = highs.pop()
                if right_high == k:
                    highs = [n for n, o in enumerate(nodes) if o == (max(nodes) - 1)]
                    if highs:
                        right_high = highs.pop()
                    elif sum(nodes) == nodes[right_high]:
                        print("Is not a graph: ", orignodes)
                        nodes = [-1 for z in range(0, len(nodes))]
                        notgraphs += 1
                    else:
                        highs = [n for n, o in enumerate(nodes) if o == (max(nodes) - 2)]
                        right_high = highs.pop()
                if (right_high + 1,k + 1) in edges and highs:
                    right_high = highs.pop()
                elif (right_high + 1,k + 1) in edges and not highs:
                    tnodes = [n for n in nodes]
                    tnodes.remove(max(tnodes))
                    highs = [n for n, o in enumerate(tnodes) if o == max(tnodes)]
                    right_high = highs.pop()
		 for m in range(right_high, k - 1, -1):
                    #print("m",m,"k",k)
                    if (m + 1,k + 1) in edges:
                        continue
                    elif nodes[k] == 0:
                        break
                    elif m == k:
                        print("Is not a graph: ", orignodes)
                        nodes = [-1 for z in range(0, len(nodes))]
                        notgraphs += 1
                    elif nodes[m] < 1:
                        continue
                    else:
                        edges.append((m + 1,k + 1))
                        nodes[k] -= 1
                        nodes[m] -= 1
                        break
            k += 1
        if sum(nodes) > -1:
            print("Vertices: ",orignodes)
            print("Edges: ",edges)
print("Total number of possible edge graphs:", numgraphs - notgraphs, "out of", numgraphs)